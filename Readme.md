# Project Four: Rust AWS Lambda and Step Functions
video link: https://youtu.be/knigMdteu9s
## Requirements
* Rust AWS Lambda function
* Step Functions workflow coordinating Lambdas
* Orchestrate data processing pipeline

## Rust AWS Lambda Function
(1) __Rust AWS Lambda Function Overview__ <br/>
This Individual Project using Rust and AWS Lambda involves reading from a DynamoDB database, which stores data on club_name and winner_rate, and then writing the results back to a different DynamoDB table. The Lambda function is triggered by an API Gateway request, which provides the club_name as input. The function reads the winner_rate corresponding to the club_name from the database and writes the results back to a new table named 'WinnerData' within DynamoDB. This setup allows for easy retrieval and viewing of each club's winner_rate from the database.

FUN FACT: These are the predictions for the winning percentage of the quarterfinals of the 23-24 UEFA Champions League soccer season。

(2) __Build and Deploy Rust Lambda Function to AWS Lambda__ <br/>
After structuring the Lambda function, build it and deploy it to AWS Lambda.
- Data preparation: Create a DynamoDB table named 'ClubData' to store the results.
AWS CLI Command to Create ClubData Table:
```bash
aws dynamodb create-table \
    --table-name ClubData \
    --attribute-definitions AttributeName=club_name,AttributeType=S \
    --key-schema AttributeName=club_name,KeyType=HASH \
    --billing-mode PAY_PER_REQUEST \
    --region us-east-1
```
Populating the ClubData Table:
```bash
aws dynamodb put-item \
    --table-name ClubData \
    --item '{
        "club_name": {"S": "Real Madrid"},
        "winner_rate": {"N": "39"}
    }' \
    --region us-east-1

aws dynamodb put-item \
    --table-name ClubData \
    --item '{
        "club_name": {"S": "Bayern"},
        "winner_rate": {"N": "27"}
    }' \
    --region us-east-1

aws dynamodb put-item \
    --table-name ClubData \
    --item '{
        "club_name": {"S": "PSG"},
        "winner_rate": {"N": "17"}
    }' \
    --region us-east-1

aws dynamodb put-item \
    --table-name ClubData \
    --item '{
        "club_name": {"S": "Dortmund"},
        "winner_rate": {"N": "18"}
    }' \
    --region us-east-1
```
![DynamoDB Items](DynamoDB.png)
- Build the function
```bash
$ cargo lambda build --release
```
- Deploy the function
```bash
// Deploy the Lambda function specifying the AWS region
$ cargo lambda deploy --region <region>
```
![AWS Lambda](lambda_deploy.png)

(3) __Attach the policy to Lambda instance__
- Click on the AWS Lambda function.
- Navigate to `Configuration` -> `Permission`.
- Click on the link labeled `Role name`.
- Include permissions for the __GetItem__ and __PutItem__ actions on the `FindBMI` database table, as well as the __PutItem__ action on the `BmiData` database table.

(4) __Test the function__ <br/>
: Test the function to ensure it is working properly. <br/>

- Test json file
```json
{
  "club_name": "Bayern"
}
```
- Result
```json
{
  "club_name": "Bayern",
  "winner_rate": "27"
}
```

## Step Functions Workflow <br/>
: AWS Step Functions orchestrate serverless workflows using AWS Lambda functions to execute tasks. By defining a sequence of steps, each invoking a Lambda function, we can manage complex workflows that include conditional logic and error handling. This integration enables seamless automation and data processing without the need to manage infrastructure.

![Step Function](step_functions.png)

## Data Processing Pipeline

(1) __Lambda Function Details__
- Describe the specific operations performed by the Rust Lambda function, such as reading user data from DynamoDB, returning the winner rate for the club.

_Example: Get Club Data_
```rust
async fn get_club_data(client: &Client, club_name: String) -> Result<Response, LambdaError> {
    let table_name = "ClubData";

    let resp = client.get_item()
        .table_name(table_name)
        .key("club_name", AttributeValue::S(club_name.clone()))
        .send()
        .await?;

    // Log the raw response from DynamoDB to verify data structure
    println!("DynamoDB Response: {:?}", resp);

    if let Some(item) = resp.item() {
        // Retrieve the winner rate as a numeric string and convert it to an owned string
        let winner_rate = item.get("winner_rate")
            .and_then(|v| v.as_n().ok()) // Get the numeric value as a string
            .map(|s| s.to_owned()) // Clone the string to own it
            .unwrap_or_else(|| "Default Rate Not Found".to_string());

        println!("Winner Rate Retrieved: {}", winner_rate); // Log the extracted winner rate
        Ok(Response { club_name, winner_rate })
    } else {
        println!("No data found for the provided club name"); // Log if no data found
        Err(LambdaError::from("No data found for the provided club name"))
    }
}
```

## References
https://www.cargo-lambda.info/guide/getting-started.html