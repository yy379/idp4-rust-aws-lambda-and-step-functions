use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, types::AttributeValue};
use lambda_runtime::{Error as LambdaError, LambdaEvent, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use simple_logger::SimpleLogger;

#[derive(Deserialize)]
struct Request {
    club_name: String,
}

#[derive(Serialize)]
struct Response {
    club_name: String,
    winner_rate: String,
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    SimpleLogger::new().with_utc_timestamps().init()?;
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    let request: Request = serde_json::from_value(event.payload)
        .map_err(|e| LambdaError::from(Box::new(e) as Box<dyn std::error::Error + Send + Sync>))?;
    
    let config = load_from_env().await;
    let client = Client::new(&config);

    let response = get_club_data(&client, request.club_name.clone()).await?;

    Ok(json!({
        "club_name": response.club_name,
        "winner_rate": response.winner_rate
    }))
}

async fn get_club_data(client: &Client, club_name: String) -> Result<Response, LambdaError> {
    let table_name = "ClubData";

    let resp = client.get_item()
        .table_name(table_name)
        .key("club_name", AttributeValue::S(club_name.clone()))
        .send()
        .await?;

    // Log the raw response from DynamoDB to verify data structure
    println!("DynamoDB Response: {:?}", resp);

    if let Some(item) = resp.item() {
        // Retrieve the winner rate as a numeric string and convert it to an owned string
        let winner_rate = item.get("winner_rate")
            .and_then(|v| v.as_n().ok()) // Get the numeric value as a string
            .map(|s| s.to_owned()) // Clone the string to own it
            .unwrap_or_else(|| "Default Rate Not Found".to_string());

        println!("Winner Rate Retrieved: {}", winner_rate); // Log the extracted winner rate
        Ok(Response { club_name, winner_rate })
    } else {
        println!("No data found for the provided club name"); // Log if no data found
        Err(LambdaError::from("No data found for the provided club name"))
    }
}





